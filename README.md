# README #

Request Slicer Application Done for WebiSaba as Back-end developer assignment for Job Application

### Description ###

The Application Contain two main Classes :

* RequestSlicer: which receive and manage high volume of requests from large number of sources.

* Client:send the request for requestSlicer class , the request contain the request text and the name of the sender as a source of the request ( I would use the Remote Address as the source but i don't have many devices to test the application on it. So it was an easy quick solution )  .

For the Database the Application use Java Db on Apache Derby Server. 

### How do I get set up? ###

* You must have netbeans IDE that come with GlassFish server registered on it .

if you don't have glassFish Server please read ([Configuring the Database](https://netbeans.org/kb/docs/ide/java-db.html#configuring)) and ([Registering the Database in NetBeans IDE](https://netbeans.org/kb/docs/ide/java-db.html#registering)) 

* Download the database from [here](https://drive.google.com/open?id=0B5NueNSGDILgaWk0MzltbldnQnc)

move it to Derby folder which is usually in this path (C:\Users\YourUser\AppData\Roaming\NetBeans\Derby)

* Go to Services on the netbeans IDE start the java DB server by right click on (java DB) then start the server.

* Expand (Java DB) , right click on (slicerRequest) Database then click on Connect.





* clone the project using this link 
https://jazzar@bitbucket.org/jazzar/nio.git

* Run The RequestSlicer class.

After running it you will see the name of top 10 sources.

* Run Client Class how many times you want .

After running it you will be asked for entering the request text and the name of the source.






## Who do I talk to? ###

* Naji Al Jazzar (naji.aljazzar@gmail.com)* *