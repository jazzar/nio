/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nio;

/**
 *
 * @author so-so
 */
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Client{
    
    
    
     public static void main(String[] args) throws Exception {
         
		Runnable client = new Runnable() {
			@Override
			public void run() {
				 try {
					 new Client().startClient();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		};
    
       new Thread(client).start();
    }
 
    public void startClient()
            throws IOException, InterruptedException {
        ByteBuffer buffer;
        byte[]  message;
        InetSocketAddress hostAddress = new InetSocketAddress("localhost", 8090);
        SocketChannel client = SocketChannel.open(hostAddress);
 
        System.out.println("Client... started");
        
        String threadName = Thread.currentThread().getName();
 
        // Send request to server
        System.out.println("Enter The Request Massege");      
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String req = br.readLine();
        req = "req "+req; 
        
        // Send source name to server
        System.out.println("Enter The Source Name");      
        String source = br.readLine();
        req= req + " name "+source;
        message = req.getBytes();
        buffer = ByteBuffer.wrap(message);
        client.write(buffer);
      
        // close the connection
        System.out.println("Succefully sent");
        client.close();            
    }
}
