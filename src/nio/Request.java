/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nio;

/**
 *
 * @author naji
 */
public class Request {
    private String host;
    private String message;
    private String RemoteAddress;

    public Request(String host, String message ,String RemoteAddress) {
        this.host = host;
        this.message = message;
        this.RemoteAddress = RemoteAddress;
    }

    public String getHost() {
        return host;
    }

    public String getMessage() {
        return message;
    }

    public String getRemoteAddress() {
        return RemoteAddress;
    }
    
}
