/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nio;

/**
 *
 * @author so-so
 */
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;
import nio.Request;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSetMetaData;



public class RequestsSlicer {
    private Selector selector;
    private Map<SocketChannel,List> dataMapper;
    private InetSocketAddress listenAddress;
    private static Connection conn = null;
    private static Statement stmt = null;
    
    public static void main(String[] args) throws Exception {
        
        topFreqSources();
                   
    	Runnable server = new Runnable() {
			@Override
			public void run() {
				 try {
					new RequestsSlicer("localhost", 8090).startServer();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		};
    new Thread(server).start();
    }

    public RequestsSlicer(String address, int port) throws IOException {
    	listenAddress = new InetSocketAddress(address, port);
        dataMapper = new HashMap<SocketChannel,List>();
    }

    // create server channel	
    void startServer() throws IOException {
        this.selector = Selector.open();
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);

        // retrieve server socket and bind to port
        serverChannel.socket().bind(listenAddress);
        serverChannel.register(this.selector, SelectionKey.OP_ACCEPT);

        System.out.println("Server started...");

        while (true) {
            // wait for events
            this.selector.select();

            //work on selected keys
            Iterator keys = this.selector.selectedKeys().iterator();
            while (keys.hasNext()) {
                SelectionKey key = (SelectionKey) keys.next();

                // this is necessary to prevent the same key from coming up 
                // again the next time around.
                keys.remove();

                if (!key.isValid()) {
                    continue;
                }

                if (key.isAcceptable()) {
                    this.accept(key);
                }
                else if (key.isReadable()) {
                    this.read(key);
                }
            }
        }
    }

    //accept a connection made to this channel's socket
    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
        SocketChannel channel = serverChannel.accept();
        channel.configureBlocking(false);
        Socket socket = channel.socket();
        SocketAddress remoteAddr = socket.getRemoteSocketAddress();
        System.out.println("Connected to: " + remoteAddr);

        // register channel with selector for further IO
        dataMapper.put(channel, new ArrayList());
        channel.register(this.selector, SelectionKey.OP_READ);
    }
    
    //read from the socket channel
    private void read(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel) key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int numRead = -1;
        
        Socket socket = channel.socket();
        SocketAddress remoteAddr = socket.getRemoteSocketAddress();
        String RemoteAddress = socket.getInetAddress().getHostAddress();
        RemoteAddress = RemoteAddress + ":" + socket.getPort();
        numRead = channel.read(buffer);

        if (numRead == -1) {
            this.dataMapper.remove(channel);
           
            System.out.println("Connection closed by client: " + remoteAddr);
            channel.close();
            key.cancel();
            return;
        }
        
        
        byte[] data = new byte[numRead];
        System.arraycopy(buffer.array(), 0, data, 0, numRead);
        String incomming = new String(data);
        String reqMsg = null;
        String host = null;
        if (incomming.indexOf("req ") >= 0) {
           reqMsg = incomming.substring(incomming.indexOf("req"),  incomming.indexOf(" name"));
           reqMsg = reqMsg.replace("req ","");
        }
        if (incomming.indexOf("name ") >= 0) {
           host = incomming.substring(incomming.indexOf(" name "),  incomming.length());
           host = host.replace("name ","");

        }
        
        Request req;
        System.out.println("reqMsg "+reqMsg);
        System.out.println("host "+host);

        if(reqMsg != null && host != null){
            req = new Request(host , reqMsg, RemoteAddress);
            
            System.out.println("Request made for : " + remoteAddr);
            this.requestHandler(req); 
        }
        
    }
    
    
    private void requestHandler(Request req){
        
        createConnection();
        
        try
        {
            stmt = conn.createStatement();
            stmt.execute("INSERT INTO APP.REQUEST ( REUESTMASSEGE, REQUESTSOURCE, REQUESTADDRESS, TIMENOW) " +
            " VALUES (\'"+req.getMessage()+"\',\'"+req.getHost()+"\',\'"+req.getRemoteAddress()+"\', CURRENT_TIMESTAMP)");
            stmt.close();
            stmt = conn.createStatement();
            ResultSet rs =stmt.executeQuery("SELECT * FROM APP.SOURCES WHERE NAME=\'"+req.getHost()+"\'");
            if (rs.next())
            {
                int val = rs.getInt(2)+1;
                stmt = conn.createStatement();
                stmt.execute("UPDATE  APP.SOURCES SET VALUE =" +val+"where NAME=\'"+req.getHost()+"\'");
            
            }
            else
            {
                stmt = conn.createStatement();
                stmt.execute("INSERT INTO APP.SOURCES (NAME) VALUES (\'"+req.getHost()+"\')");
            }
          
            
            stmt.close();
            conn.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
        
    
    }
    
    private static void topFreqSources(){
       
        
        createConnection();
        
        try
        {
            stmt = conn.createStatement();
            ResultSet rs =stmt.executeQuery(" select *  from APP.SOURCES order by value desc FETCH NEXT 10 ROWS ONLY");
            System.out.print("Top Sources : \n");
            int i = 1;
            while (rs.next())
            {
                System.out.print(i +". "+rs.getString(1)+"\n");
                i++;
            }
            stmt.close();
            conn.close();
        }
        catch (SQLException sqlExcept)
        {
            sqlExcept.printStackTrace();
        }
        
    }
    
    private static void createConnection()
    {
        
        String url = "jdbc:derby://localhost:1527/";
        String dbName = "slicerRequest";
        String driver = "org.apache.derby.jdbc.ClientDriver";
        String userName = "naji"; 
        String password = "admin";
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url+dbName,userName,password);
            
  } catch (Exception e) {
  e.printStackTrace();
  }
   
    }
    
    
}

